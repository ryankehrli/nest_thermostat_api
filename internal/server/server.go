package server

import (
	"bitbucket.org/ryankehrli/nest_thermostat_api/internal/server/api"
	"github.com/gin-gonic/gin"
)

func Run() error {

	//	Default gin router
	router := gin.Default()

	// Routes
	api.Routes(router)

	//	Start server on port localhost:8080
	router.Run(":8080")
	return nil
}
