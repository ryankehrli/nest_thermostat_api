package api

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/ryankehrli/nest_thermostat_api/internal/auth2"
	"github.com/gin-gonic/gin"
)

var tokenObj = auth2.New()

func StructureRoutes(router *gin.Engine) {
	router.GET("/structures/subscribe", func(c *gin.Context) {
		Subscribe(c)
	})
}

//	Subscribe to changes | For more Info visit: https://developers.nest.com/guides/api/how-to-read-data
func Subscribe(c *gin.Context) {
	url := "https://developer-api.nest.com/"
	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		fmt.Println("An error occured contacting the ness api")
		return
	}

	req.Header.Add(
		"Authorization",
		fmt.Sprintf("Bearer %s", tokenObj.Use()),
	)

	customClient := http.Client{
		CheckRedirect: func(redirRequest *http.Request, via []*http.Request) error {
			// Go's http.DefaultClient does not forward headers when a redirect 3xx
			// response is received. Thus, the header (which in this case contains the
			// Authorization token) needs to be passed forward to the redirect
			// destinations.
			redirRequest.Header = req.Header

			// Go's http.DefaultClient allows 10 redirects before returning an
			// an error. We have mimicked this default behavior.
			if len(via) >= 10 {
				return errors.New("stopped after 10 redirects")
			}
			return nil
		},
	}

	response, err := customClient.Do(req)

	if err != nil {
		c.JSON(500, gin.H{
			"message": "to many redirects, ness requires 10 or less",
		})
		fmt.Println("Error redirecting")
		return
	}

	if response.StatusCode != 200 {
		c.JSON(response.StatusCode, gin.H{
			"message": "there was an error subscribing to the ness service",
			"code":    response.StatusCode,
			"token":   tokenObj, //	May not want to return this value
		})
		panic(fmt.Sprintf(
			"Expected a 200 status code; got a %d",
			response.StatusCode,
		))
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("An error occured reading the subscription body")
	}
	fmt.Println(string(body))
}

//	Structure Requests **MAYBE USELESS**
// func GetThermostats(c *gin.Context) {
// 	response, err := http.Get("https://developer-api.nest.com/structures/my_id/thermostats") //Needs the id of the device

// 	if err != nil {
// 		fmt.Printf("The HTTP request failed server side with error %s\n", err)
// 		return
// 	}

// 	data, err := ioutil.ReadAll(response.Body)
// 	defer response.Body.Close()

// 	if err != nil {
// 		fmt.Printf("Failed to read response %v", err)
// 	}

// 	mydata := string(data)
// 	fmt.Println(mydata)

// 	//Checking for return errors
// 	if response.StatusCode != 200 {
// 		c.JSON(response.StatusCode, gin.H{
// 			"error":      "Ness server returned error.",
// 			"ness_error": mydata,
// 		})
// 		fmt.Printf(
// 			"Expected a 200 status code: got a %d",
// 			response.StatusCode,
// 		)
// 		return
// 	}
// 	//	If good
// 	c.JSON(200, gin.H{
// 		"data": mydata,
// 	})
// }
