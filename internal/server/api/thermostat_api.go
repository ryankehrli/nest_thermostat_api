package api

import (
	"errors"

	"github.com/gin-gonic/gin"
)

//** This file contains http functions for setting a nest thermostat **\\

var url = "https://developer-api.nest.com/devices/thermostats"

//	My Routes
func ThermostatRoutes(router *gin.Engine) {
	router.PUT("/thermostat/:id", func(c *gin.Context) {
		//do something
	})
}

//	Thermostat Put Methods
func Set_HVAC_Mode(device_id string, mode string, c *gin.Context) error {
	if mode != "heat" || mode != "cool" || mode != "heat-cool" || mode != "eco" || mode != "off" {
		err := errors.New("Invalid HVAC Setting")
		return err
	}
	myurl := url, device_id,
		putRequest()
	return nil
}
func Set_Temp_F(c *gin.Context) {

}
func Set_Temp_C(c *gin.Context) {

}
func Set_Target_Temperature_F(c *gin.Context) {

}
func Set_Target_Temperature_C(c *gin.Context) {

}
