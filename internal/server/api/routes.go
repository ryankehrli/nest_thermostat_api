package api

import (
	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {
	ThermostatRoutes(router)
	StructureRoutes(router)
}
