package nest

import (
	"errors"
	"log"
	"os"

	"bitbucket.org/ryankehrli/nest_thermostat_api/internal/auth2"
)

type Nest struct {
	AccessToken   *auth2.AccessToken
	AUTH_CODE     string
	CLIENT_ID     string
	CLIENT_SECRET string
	Data          string
}

func New() *Nest {
	token := auth2.New()
	return &Nest{
		AccessToken:   token,
		AUTH_CODE:     "",
		CLIENT_ID:     "",
		CLIENT_SECRET: "",
		Data:          "",
	}
}

func (nest *Nest) SetGlobalVars() {
	authCode, exists := os.LookupEnv("AUTH_CODE")
	if !exists {
		log.Fatal(errors.New("AUTH_CODE not set"))
		return
	}
	client_id, exists := os.LookupEnv("CLIENT_ID")
	if !exists {
		log.Fatal(errors.New("CLIENT_ID not set"))
		return
	}
	client_secret, exists := os.LookupEnv("CLIENT_SECRET")
	if !exists {
		log.Fatal(errors.New("CLIENT_SECRET not set"))
		return
	}
	nest.AUTH_CODE = authCode
	nest.CLIENT_ID = client_id
	nest.CLIENT_SECRET = client_secret
}
