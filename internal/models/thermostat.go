package models

type Thermostat struct {
	//	Read values
	Name            string `json:"name"`
	WhereID         string `json:"where_id"`
	StructureID     string `json:"structure_id"`
	IsOnline        string `json:"is_online"`
	SoftwareVersion string `json:"software_version"`

	// **You Cannot Set Targets in Eco Mode.**
	//	Eco Mode Temperatures As of Writing Are Handeled by Ness.
	EcoTemperatureLowf  int     `json:"eco_temperature_low_f"`
	EcoTemperatureHighf int     `json:"eco_temperature_high_f"`
	EcoTemperatureLowc  float64 `json:"eco_temperature_low_c"`
	EcoTemperatureHighc float64 `json:"eco_temperature_high_c"`

	//	Read/Write values
	HVACMode         string `json:"hvac_mode"`
	TemperatureScale string `json:"temperature_scale"`
	//	cool or heat
	TargetTempF int     `json:"target_temperature_f"`
	TargetTempC float64 `json:"target_temperature_c"`
	//	heat-cool
	TargetTemperatureLowf  int     `json:"target_temperature_low_f"`
	TargetTemperatureHighf int     `json:"target_temperature_high_f"`
	TargetTemperatureLowc  float64 `json:"target_temperature_low_c"`
	TargetTemperatureHighc float64 `json:"target_temperature_high_c"`
	//	room temp
	FAmbientTemp int `json:"ambient_temperature_f"`
	CAmbientTemp int `json:"ambient_temperature_c"`
}

//type Option func(*Thermostat)

//	Junky functions
// func Set_HVAC_Mode(mode string) (Option, error) {
// 	mymode := ""
// 	if mode == "heat" || mode == "cool" || mode == "heat-cool" ||
// 		mode == "eco" || mode == "off" {
// 		mymode = mode
// 	} else {
// 		return nil, errors.New("Invalid HVAC Setting")
// 	}

// 	return func(t *Thermostat) {
// 		t.HVAC_mode = mymode
// 	}, nil
// }

// func Set_Temperature_F(tempf int) Option {
// 	return func(t *Thermostat) {
// 		t.TargetTempF = tempf
// 	}
// }

// func Set_Temperature_C(tempc float64) Option {
// 	return func(t *Thermostat) {
// 		t.TargetTempC = tempc
// 	}
// }
