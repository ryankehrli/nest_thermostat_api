package auth2

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type AccessToken struct {
	Token       string
	TimesUsed   int
	LastUpdated time.Time
	LastUsed    time.Time
}

const (
	AUTH_CODE     = "something"
	CLIENT_ID     = "something"
	CLIENT_SECRET = "something"
)

//	Limits the token use rate to once per minute
func (token *AccessToken) RateLimiter() {
	diff := time.Since(token.LastUsed)
	if diff < time.Minute {
		time.Sleep(diff) //	Could add additional functionality like a que
	}
}

//	Call this to generate a new token object <-- when used it will request a new token from nest
func New() *AccessToken {
	return &AccessToken{
		"",
		0,
		time.Now().AddDate(-3, 0, 0),
		time.Now().AddDate(-1, 0, 0),
	}
}

//	Call this to use the token, it checks it's validity and limits the rate. If invalid it will request a new token.
func (token *AccessToken) Use() string {
	if token.IsValid() {
		token.RateLimiter()
		token.TimesUsed++
		return token.Token
	} else {
		fmt.Println("Requesting a new token at :", time.Now())
		token.Request_Token()
	}
	token.TimesUsed++
	return token.Token
}

//	Checks to see if we have a valid access token.
func (token *AccessToken) IsValid() bool {
	if token.LastUpdated.IsZero() {
		return false
	}
	now := time.Now()
	duration := now.Sub(token.LastUpdated)
	if duration < time.Minute*10 {
		return false
	}
	if 10 >= token.TimesUsed {
		return false
	}
	return true
}

//	This will go and get a new token if we need it
func (token *AccessToken) Request_Token() {

	url := "https://api.home.nest.com/oauth2/access_token"

	builder := fmt.Sprintf("code=%s&client_id=%s&client_secret=%s&grant_type=authorization_code",
		AUTH_CODE, CLIENT_ID, CLIENT_SECRET)

	payload := strings.NewReader(builder)

	req, err := http.NewRequest("POST", url, payload)

	if err != nil {
		fmt.Printf("There was an error requesting the tocken %v", err)
		return
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		fmt.Printf("There was an error requesting the tocken %v", err)
		return
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		fmt.Printf("Error reading the response %v", err)
		return
	}
	//	Updating Token Values
	token.LastUpdated = time.Now()
	token.TimesUsed = 0
	token.Token = string(body)
	token.LastUsed = time.Now()
}
