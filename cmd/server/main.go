package main

import (
	"log"

	"bitbucket.org/ryankehrli/nest_thermostat_api/internal/server"
)

func main() {
	if err := server.Run(); err != nil {
		log.Fatal(err)
	}
}
